﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArrowController : MonoBehaviour {
	/// <summary>
	/// The arrow.
	/// </summary>
	public static  ArrowController Reference;

	/// <summary>
	/// The score.
	/// </summary>
	public Text Score;

	/// <summary>
	/// The score.
	/// </summary>
	public int PlayerScore;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	public void Awake(){

		Reference = this;
        //update score
        UpdateScore(0);
        
	}

	/// <summary>
	/// The arrow.
	/// </summary>
	public GameObject Arrow;

	/// <summary>
	/// Creates the arrow.
	/// </summary>
	public void CreateArrow(float withdelay){

		Debug.Log ("Request for instantiation");
        

        Invoke ("CreateArrow", withdelay);
	}

	/// <summary>
	/// Creates the arrow.
	/// </summary>
	private void CreateArrow(){

        //create the arrow.
        var myarrow = Instantiate(Arrow);
		//reset all transform
		myarrow.transform.localPosition = Vector3.zero;
		//reset angle
		myarrow.transform.localRotation =Quaternion.identity;
		//rename it.
		myarrow.name = "New Arrow";
        //update the move.
        TrgetMovementController.Reference.Move();


    }

	/// <summary>
	/// Updates the score.
	/// </summary>
	public void UpdateScore(int value ){

		PlayerScore = value;

		Score.text = "Score:"+PlayerScore.ToString ();
	}

}
