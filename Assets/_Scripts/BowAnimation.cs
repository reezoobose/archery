﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BowAnimation : MonoBehaviour {

    /// <summary>
    /// Spped of moving.
    /// </summary>
    [Range(1,10)]
    public float Speed;

    /// <summary>
    /// Screen Height .
    /// </summary>
    public float _screenHeight;

    /// <summary>
    /// Upper Bound of the Archary .
    /// </summary>
    private float _upperBound;

    /// <summary>
    /// LowerBound of the Archary .
    /// </summary>
    private float _lowerBound;

    /// <summary>
    /// Current transform .
    /// </summary>
    private Transform _currentTransForm;

    /// <summary>
    /// Awake the instance.
    /// </summary>
    private void Awake()
    {
       
        //Get _upper Bound.
        _upperBound = _screenHeight / 2;
        //Get _Lower Bound.
        _lowerBound = -_upperBound;
        Debug.Log("Upper Bound and Lower Bound Calculated : " + _upperBound + "----------" + _lowerBound);
        //Current TransForm.
        _currentTransForm = this.gameObject.transform;
    }
       

    /// <summary>
    /// Start the Instance.
    /// </summary>
    /// <returns></returns>
    private IEnumerator Start()
    {
        //Make it ready for going up.
        var positionInY = _upperBound;
        //Going Up || Going Down
        while( (_currentTransForm.position.y < positionInY) || (_currentTransForm.position.y > positionInY) )
        {
            //Going up condition
           if (_currentTransForm.position.y < positionInY){
                //Updated the spped .
                var UpdatedY = _currentTransForm.position.y + Speed * Time.deltaTime;
                //Apply the speed.
                _currentTransForm.position = new Vector3(0f, UpdatedY, 0f);
            }

            //If it reaches to upper bound
            if(_currentTransForm.position.y >= _upperBound)
            {
                //Just reverse the direction
                positionInY = _lowerBound;

            }

            //Going down condition
            if (_currentTransForm.position.y > positionInY)
            {
                //Updated the spped .
                var UpdatedY = _currentTransForm.position.y - Speed * Time.deltaTime;
                //Apply the speed.
                _currentTransForm.position = new Vector3(0f, UpdatedY, 0f);
            }

            //If it reaches to Lower bound
            if (_currentTransForm.position.y <= _lowerBound)
            {
                //Just reverse the direction
                positionInY = _upperBound;

            }

            yield return null;
        }
        //Iterate to the Upeer Bound
        
       

    }
}
