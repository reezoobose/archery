﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ArrowCollisionController : MonoBehaviour {

	/// <summary>
	/// Raises the collision enter2 d event.
	/// </summary>
	/// <param name="coll">Coll.</param>
	void OnCollisionEnter2D(Collision2D coll) {
		Debug.Log ("Collided with " + coll.gameObject.name);
		//If collidede with Target just destroy.
		if (coll.gameObject.tag == "Target") {
			//arrow have completed its run
			Arrow.Reference.CurrentCondition = Arrow.ArrowCondition.TargetHitted;
			//Deactivate the collider to avoid Collision with wall.
			this.gameObject.GetComponent<BoxCollider2D>().enabled =false ;
			//create an duplicate arrow.
			ArrowController.Reference.CreateArrow (1f);
			Destroy (this.transform.parent.gameObject, 1f);
           


		}
		//If collidede with Wastage just destroy.
		if (coll.gameObject.tag == "Wastage") {
            //arrow have completed its run
            Arrow.Reference.CurrentCondition = Arrow.ArrowCondition.TargetHitted;
            //create an duplicate arrow.
            ArrowController.Reference.CreateArrow (0);
			Destroy (this.transform.parent.gameObject);
		}

	}

}
