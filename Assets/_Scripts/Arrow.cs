﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Arrow : MonoBehaviour {

    /// <summary>
    /// Condition of arrow.
    /// </summary>
    public enum ArrowCondition
    {
        ReadyToThrow=0,
        RunningForTheTarget=1,
        TargetHitted=2,
    };

    /// <summary>
    /// Arrow Current Condition.
    /// </summary>
    public ArrowCondition CurrentCondition;

	/// <summary>
	/// Arrow speed.
	/// </summary>
	public  float SpeedOfArrow;
	/// <summary>
	/// The distance.
	/// </summary>
	public float distance;
	/// <summary>
	/// The position.
	/// </summary>
	private Transform rectTransForm ;
	/// <summary>
	/// The reference.
	/// </summary>
	public static Arrow Reference;

    /// <summary>
    /// Follower.
    /// </summary>
    public GameObject Follower;

	/// <summary>
	/// Awake this instance.
	/// </summary>
	private void Awake(){
		
        //Transform of the object.
		rectTransForm = this.gameObject.transform;
        //Static reference.
		Reference = this;
        //Follower
        Follower = GameObject.FindGameObjectWithTag("Follower");
        //Current condition.
        CurrentCondition = ArrowCondition.ReadyToThrow;
	}

	/// <summary>
	/// Move the arrow in forward direction.
	/// </summary>
	public void Move()
	{
		
		var positionInX = rectTransForm.position.x;
		var updatedPosition = positionInX + distance;
		rectTransForm.position = Vector3.Lerp (rectTransForm.position, new Vector3 (updatedPosition, rectTransForm.position.y, 0f), SpeedOfArrow * Time.deltaTime);
	}

	/// <summary>
	/// Update this instance.
	/// </summary>
	public void Update(){


		//On mouse button down.
		if (Input.GetMouseButtonDown (0) && CurrentCondition== ArrowCondition.ReadyToThrow) {
            //Arrow will  run.
            CurrentCondition = ArrowCondition.RunningForTheTarget;
		}

		//Move the arrow now.
		if (CurrentCondition == ArrowCondition.RunningForTheTarget) {
			Move ();
		}

        //Follow the follower only when it is rady to run.
        if (CurrentCondition == ArrowCondition.ReadyToThrow)
        {
            this.gameObject.transform.position = Follower.transform.position;
        }

	}




}
