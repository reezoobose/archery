﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrgetMovementController : MonoBehaviour
{
    public Transform Center;
    public Transform Up;
    public Transform Below;
    public static TrgetMovementController Reference;
    public float TargetMovementSpeed;
    public bool IsTargetMoving;

    private void Awake()
    {
        Reference = this;
    }

    private void UpdateBounds(float value)
    {
        Center.position = new Vector3(Center.position.x, Center.position.y + value);
        Up.position = new Vector3(Up.position.x, Up.position.y + value);
        Below.position = new Vector3(Below.position.x, Below.position.y + value);
    }

    /// <summary>
    /// Move tha Target.
    /// </summary>
    public void Move()
    {
        //Bounds
        const float upperBound = 3f;
        const float lowerBound = -3f;
        //genrate a number.
        var randomnum = Random.Range(upperBound, lowerBound);
        //store the Y position.
        var storedYposition = gameObject.transform.position.y;
        //Chcek the difference with the previous .
        var differenceWithPrevious = randomnum > 0 ? randomnum - storedYposition : -(storedYposition - randomnum);
        //Debug the output
        Debug.Log("differenceWithPrevious ===" + differenceWithPrevious);
        //update bounds
        UpdateBounds(differenceWithPrevious);
        //move to position
        this.gameObject.transform.position = new Vector3(0f, randomnum);
    }

}