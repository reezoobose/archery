﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCalculator : MonoBehaviour {

    //Boiunds.
    public GameObject Center;
    public GameObject UpperBound;
    public GameObject LowerBound;


    /// <summary>
    /// Collision Event.
    /// </summary>
    /// <param name="collision"></param>
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if collided with Arrow
        if (collision.gameObject.CompareTag("Arrow")){
            //Find the position of the collision.
            var collisionPointY = collision.contacts[0].point.y;
            //Y direction.
            Debug.Log("Contact received at " + collisionPointY);

            //update score
            ArrowController.Reference.UpdateScore(CalculateScore(collisionPointY));
         }
    }


    

    /// <summary>
    /// Calculate score.
    /// </summary>
    /// <param name="hitPosition"></param>
    /// <returns></returns>
    private int CalculateScore(float hitPosition)
    {
        //Score range.
        var maxScore = 10;


        //if hit position is positive
        if (hitPosition > 0)
        {
            //currnt center y
            var center = Center.transform.localPosition.y;
            //Log
            Debug.Log("Current center = " + center);
            //length of +ve  y
            var length = UpperBound.transform.localPosition.y - center;
            //Log
            Debug.Log("Length" + length);
            //find difference of hit point and center .
            //that indicate how far it is .
            var diff = Mathf.Abs(hitPosition - center);
            //log
            Debug.Log("Diff with center " + diff);
            //percentage recahed with res pect to Upperbound
            var percentage = (diff / length) * 100;
            //log
            Debug.Log("calculated percentage" + percentage);
            Debug.Log("Max scores " + (100 - percentage) + "will be given");
            //The more percentage indicate more far from center.
            maxScore = (int)(maxScore * ((100 - percentage) / 100));
            //return
            return maxScore;
        }


        //if hit position is Negative
        if (hitPosition < 0)
        {
            //currnt center y
            var center = Center.transform.localPosition.y;
            //Log
            Debug.Log("Current center = " + center);
            //length of -ve  y
            var length = center - LowerBound.transform.localPosition.y;
            //Log
            Debug.Log("Length" + length);
            //find difference of hit point and center .
            //that indicate how far it is .
            var diff = Mathf.Abs(hitPosition - center);
            //log
            Debug.Log("Diff with center " + diff);
            //percentage recahed with res pect to Upperbound
            var percentage = (diff / length) * 100;
            //log
            Debug.Log("calculated percentage" + percentage);
            Debug.Log("Max scores " + (100 - percentage) + "will be given");
            //The more percentage indicate more far from center.
            maxScore = (int)(maxScore * ((100 - percentage) / 100));
            //return
            return maxScore;

        }


        if (hitPosition == 0)
        {
            return maxScore;
        }


        return 0;
    }
        

}
